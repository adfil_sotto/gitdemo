public class ApplyDCContractClass {
	// Remove the OpportunityLineItems DC lookup by Opportunity
	public static void RemoveOppItemsDC(Set<Id> opps) {
		// remove
		RemoveOpportunityLineItemsDC([Select Id, Discount, Discount_Commission__c, Affiliate_Commision__c, Customer_Discount__c From OpportunityLineItem Where OpportunityId in :opps and Discount_Commission__c != null]);		
	}
	
	// Update the OpportunityLineItems DC lookup
	public static void UpdateOppItemsDC(Map<Id, Id> mapOppDCc) {
		List<OpportunityLineItem> oppItems = [Select Id, OpportunityId, PricebookEntry.Product2Id, PricebookEntry.Pricebook2Id, Discount_Commission__c, Discount, Affiliate_Commision__c, Customer_Discount__c From OpportunityLineItem Where OpportunityId in :mapOppDCc.keySet()];
    	if (!oppItems.isEmpty()) {
        	// get dc
        	Map<Id, Map<String, Discount_Commission__c>> mapContractDC = new Map<Id, Map<String, Discount_Commission__c>>();
        	for (Discount_Commission__c d : [Select Id, Discount_Commission_Contract__c, Product__c, PriceBookId__c, Affiliate_Commision__c, Customer_Discount__c From Discount_Commission__c where Discount_Commission_Contract__c in :mapOppDCc.values()]) {
	            if (mapContractDC.get(d.Discount_Commission_Contract__c) == null) {
	                mapContractDC.put(d.Discount_Commission_Contract__c, new Map<String, Discount_Commission__c>());
	            }
            	mapContractDC.get(d.Discount_Commission_Contract__c).put(String.valueOf(d.Product__c)+d.PriceBookId__c, d);
        	}    
        	System.debug('mapContractDC: ' + mapContractDC);
        	if (!mapContractDC.isEmpty()) {        		        		
                // process & update here
                UpdateOpportunityLineItemsDC(oppItems, mapOppDCc, mapContractDC, true);													                
        	}
    	}
	}
	
	// Update the opportunity's DC Contract lookup and return the map between opportunity and DC contract
	public static void UpdateOpportunityDCContract(List<Opportunity> opps, Set<Id> actIds) {		
		// get dc contract per account
        Map<Id, List<Discount_Commission_Contract__c>> mapAcctDCc = new Map<Id, List<Discount_Commission_Contract__c>>();
        for (Discount_Commission_Contract__c dc : [Select Id, Affiliate_Partner__c, Start_Date__c, End_Date__c, Type__c From Discount_Commission_Contract__c where Affiliate_Partner__c in :actIds Order by Type__c desc, Start_Date__c]) {
            if (mapAcctDCc.get(dc.Affiliate_Partner__c) == null) {
                mapAcctDCc.put(dc.Affiliate_Partner__c, new List<Discount_Commission_Contract__c>());
            }
            mapAcctDCc.get(dc.Affiliate_Partner__c).add(dc);
        }    
        System.debug('mapAcctDCc : ' + mapAcctDCc);
        
        if (!mapAcctDCc.isEmpty()) {
            for (Opportunity op : opps) {
            	if (mapAcctDCc.containsKey(op.AccountId)) {
	                List<Discount_Commission_Contract__c> dccList = mapAcctDCc.get(op.AccountId);
	                for (Discount_Commission_Contract__c dcc : dccList) {
	                    if (op.CloseDate >= dcc.Start_Date__c && op.CloseDate <= dcc.End_Date__c) {
	                        op.Discount_Commission_Contract__c = dcc.Id;	                                                    
	                        if (dcc.Type__c == 'Campaign') {	                        	
	                            break;
	                        }
	                    }
	                }
            	} else {
            		// remove dc contract lookup
            		op.Discount_Commission_Contract__c = null;
            	}
            }
        }              
	}
	
	// Remove the OpportunityLineItems DC lookup by DC
	public static void RemoveOppItemsDCByDC(Set<Id> dcIds) {
		// remove		
		RemoveOpportunityLineItemsDC([Select Id, Discount_Commission__c, Discount, Affiliate_Commision__c, Customer_Discount__c From OpportunityLineItem Where Discount_Commission__c in :dcIds]);					
	}
	
	// apply DC to OpportunityLineItems
	public static void ApplyDCToOppItems(List<Discount_Commission__c> DCList) {
		Map<Id, Map<String, Discount_Commission__c>> mapContractDC = new Map<Id, Map<String, Discount_Commission__c>>(); 		
		for (Discount_Commission__c dc : DCList) {
			if (mapContractDC.get(dc.Discount_Commission_Contract__c) == null) {
				mapContractDC.put(dc.Discount_Commission_Contract__c, new Map<String, Discount_Commission__c>());
			}
			mapContractDC.get(dc.Discount_Commission_Contract__c).put(String.valueOf(dc.Product__c)+dc.PriceBookId__c, dc);
		}
		
		// get all Opportunity & DC contract
		Map<Id, Id> mapOppDCc = new Map<Id, Id>();
		for (Opportunity opp : [Select Id, Discount_Commission_Contract__c From Opportunity Where Discount_Commission_Contract__c in :mapContractDC.keySet()]) {
			mapOppDCc.put(opp.Id, opp.Discount_Commission_Contract__c);			
		}
		
		// get opportunitylineitems
		List<OpportunityLineItem> oppItems = [Select Id, OpportunityId, PricebookEntry.Product2Id, PricebookEntry.Pricebook2Id, Discount_Commission__c, Discount, Affiliate_Commision__c, Customer_Discount__c From OpportunityLineItem Where OpportunityId in :mapOppDCc.keySet()];
		 
		// update & process here 
		UpdateOpportunityLineItemsDC(oppItems, mapOppDCc, mapContractDC, false); 													 	
	}
	
	// Remove the Opportunity DC Contract lookup
	public static void RemoveOpportunityDCC(Set<Id> dccIds) {
		List<Opportunity> oppToUpdate = new List<Opportunity>();
		// get all related opportunity and remove the lookup
		for (Opportunity opp : [Select Id, Discount_Commission_Contract__c From Opportunity Where Discount_Commission_Contract__c in :dccIds]) {
			oppToUpdate.add(new Opportunity(Id=opp.Id, Discount_Commission_Contract__c=null));			
		}
		// update here
		if (!oppToUpdate.isEmpty()) {
			update oppToUpdate;
		}
	}
	
	// Apply DC Contract lookup to Opportunity
	public static void ApplyDCCToOpportunity(List<Discount_Commission_Contract__c> dccList) {
		Map<Id, List<Discount_Commission_Contract__c>> mapDCc = new Map<Id, List<Discount_Commission_Contract__c>>(); 
		for (Discount_Commission_Contract__c dcc : dccList) {
			if (mapDCc.get(dcc.Affiliate_Partner__c) == null) {
				mapDCc.put(dcc.Affiliate_Partner__c, new List<Discount_Commission_Contract__c>());
			}
			mapDCc.get(dcc.Affiliate_Partner__c).add(dcc);
		}
		
		Map<Id, Id> mapOppDCc = new Map<Id, Id>();
		// get all opportunity 
		for (Opportunity  opp : [Select Id, AccountId, CloseDate From Opportunity Where AccountId in :mapDCc.keySet()]) {
			for (Discount_Commission_Contract__c dcc : mapDCc.get(opp.AccountId)) {
				if (opp.CloseDate >= dcc.Start_Date__c && opp.CloseDate <= dcc.End_Date__c) {
					mapOppDCc.put(opp.Id, dcc.Id);
					if (dcc.Type__c == 'Campaign') {                        
                        break;
                    }
				}
			}
		}
		
		if (!mapOppDCc.isEmpty()) {
			List<Opportunity> oppToUpdate = new List<Opportunity>();
			Set<Id> oppIds = mapOppDCc.keySet();
			for (Id i : oppIds) {
				oppToUpdate.add(new Opportunity(Id=i, Discount_Commission_Contract__c=mapOppDCc.get(i)));
			}
			// update here
			if (!oppToUpdate.isEmpty()) {
				update oppToUpdate;			
			}
		}
	}
	
	// Remove the OpportunityLineItems DC information & computation
	private static void RemoveOpportunityLineItemsDC(List<OpportunityLineItem> oppItems) {
		List<OpportunityLineItem> oppItemsToUpdate = new List<OpportunityLineItem>();
        for (OpportunityLineItem item : oppItems) {
			OpportunityLineItem oi = new OpportunityLineItem(Id=item.Id, Discount_Commission__c=null);			        			
			Decimal disc = item.Discount;
			if (item.Affiliate_Commision__c != null)
				disc -= item.Affiliate_Commision__c;
			if (item.Customer_Discount__c != null)
				disc -= item.Customer_Discount__c;
			if (disc < 0) {
				disc = 0;
			}	
			oi.Discount = disc;		  	
			oi.Description = '';
			oi.Affiliate_Commision__c = 0;
			oi.Customer_Discount__c = 0;        	            
            oppItemsToUpdate.add(oi);
        }
        if (!oppItemsToUpdate.isEmpty()) {
            update oppItemsToUpdate;
        }
	}
	
	// Update OpportunityLineItems DC information & computation
	private static void UpdateOpportunityLineItemsDC(List<OpportunityLineItem> oppItems, 
													 Map<Id, Id> mapOppDCc, 
													 Map<Id, Map<String, Discount_Commission__c>> mapContractDC,
													 Boolean updateOtherOppItem) {		
        List<OpportunityLineItem> oppItemsToUpdate = new List<OpportunityLineItem>();
        for (OpportunityLineItem i : oppItems) {
        	if (mapOppDCc.containsKey(i.OpportunityId)) {
        		Id dcConId = mapOppDCc.get(i.OpportunityId);
        		if (mapContractDC.containsKey(dcConId)) {
        			OpportunityLineItem oi;                			
        			Map<String, Discount_Commission__c> mapContractDCValues = mapContractDC.get(dcConId);
        			if (mapContractDCValues.containsKey(String.valueOf(i.PricebookEntry.Product2Id)+String.valueOf(i.PricebookEntry.Pricebook2Id))) {
        				Discount_Commission__c dc = mapContractDCValues.get(String.valueOf(i.PricebookEntry.Product2Id)+String.valueOf(i.PricebookEntry.Pricebook2Id));
        				oi = new OpportunityLineItem(Id=i.Id, Discount_Commission__c=dc.Id); 
        				 
        				if (oi.Discount == null) { // discount to 0 if null
	                    	oi.Discount = 0;
	                	}
	                                                                                                    
	                	String descr = '';
	                    // affiliate commision
	                    if (dc.Affiliate_Commision__c != null && dc.Affiliate_Commision__c != 0) {
	                        oi.Discount += dc.Affiliate_Commision__c;                        
	                        descr = 'Affiliate Commision = ' + String.valueOf(dc.Affiliate_Commision__c);                                
	                    }                            
	                    // customer discount
	                    if (dc.Customer_Discount__c != null && dc.Customer_Discount__c != 0) {
	                        oi.Discount += dc.Customer_Discount__c;                        
	                        if (descr != '') descr+= '; '; 
	                        descr += 'Customer Discount = ' + String.valueOf(dc.Customer_Discount__c);
	                    }            
	                    // check if discount > 100
	                    if (oi.Discount > 100) {
	                    	oi.Discount = 100;
	                    }                
	                	oi.Description = descr;                	
	                	oi.Affiliate_Commision__c = dc.Affiliate_Commision__c;
	                	oi.Customer_Discount__c = dc.Customer_Discount__c;
        				oppItemsToUpdate.add(oi);                				
        			} else { // product & pricebook not exist on DC
        				if (updateOtherOppItem) {
	        				oi = new OpportunityLineItem(Id=i.Id, Discount_Commission__c=null);
	        				oi.Description = '';        				
	        				Decimal disc = i.Discount;
							if (i.Affiliate_Commision__c != null)
								disc -= i.Affiliate_Commision__c;
							if (i.Customer_Discount__c != null)
								disc -= i.Customer_Discount__c;
							oi.Discount = disc;		  	
							oi.Affiliate_Commision__c = 0;
							oi.Customer_Discount__c = 0;
							oppItemsToUpdate.add(oi); 
        				}
        			}   
        			// add to items        			            		
        		}
        	}
        }
        // update oppitems here
        if (!oppItemsToUpdate.isEmpty()) {
        	update oppItemsToUpdate;
        }
	}
}