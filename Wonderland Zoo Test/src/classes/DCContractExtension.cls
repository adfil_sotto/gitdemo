public class DCContractExtension {
    private Discount_Commission_Contract__c dcContract;
    public Discount_Commission_Contract__c cloneContract {get; set;}
    
    public String searchText {get; set;}   
    public List<Discount_Commission__c> DCs {get; set;} 
    public List<Discount_Commission__c> SelectedDCs {get; set;}
    public Boolean isDCEdit {get; set;}
    
    public Integer totalPages {get; set;}         
    public Integer pageNumber {get; set;}
    public Integer pageSize {get; set;}
    private Integer noOfRecords {get; set;}
    
    private String sortDirection = 'ASC';
    private String sortExpression = 'Name';
    
    public DCContractExtension(ApexPages.StandardController controller) {
        dcContract = (Discount_Commission_Contract__c)controller.getRecord();        
        if (dcContract.Id != null) {                        
            List<Discount_Commission_Contract__c> dcContracts = [Select Name, OwnerId, Affiliate_Partner__c, Start_Date__c, End_Date__c, Type__c From Discount_Commission_Contract__c Where id = :dcContract.Id];
            if (!dcContracts.isEmpty()) {
                dcContract = dcContracts[0];
                //if (ApexPages.currentPage().getParameters().get('clone') != null) {
                    cloneContract = new Discount_Commission_Contract__c();    
                    cloneContract.Name = dcContract.Name;
                    cloneContract.OwnerId = dcContract.OwnerId;
                    cloneContract.Affiliate_Partner__c = dcContract.Affiliate_Partner__c;
                    cloneContract.Start_Date__c = dcContract.Start_Date__c;
                    cloneContract.End_Date__c= dcContract.End_Date__c;
                    cloneContract.Type__c = dcContract.Type__c;
                //}
            }
        }    
        
        pageSize = 15;     
        totalPages = 0;
        searchText = '';
        isDCEdit = false;
    }

    private Boolean SaveNow() {
        Boolean isSaved = false;
        Savepoint sp = Database.setSavepoint();
        try {
            insert cloneContract;
            System.debug('cloneContract : ' + cloneContract);
            List<Discount_Commission__c> dcToInsert = new List<Discount_Commission__c>();
            for (Discount_Commission__c dc : [Select Discount_Commission_Contract__c, Affiliate_Commision__c, Customer_Discount__c, PriceBookId__c, Product__c From Discount_Commission__c Where Discount_Commission_Contract__c = :dcContract.Id]) {                                
                dcToInsert.add(new Discount_Commission__c(
                   Discount_Commission_Contract__c = cloneContract.Id,
                   Affiliate_Commision__c = dc.Affiliate_Commision__c,
                   Customer_Discount__c = dc.Customer_Discount__c,
                   PriceBookId__c = dc.PriceBookId__c,
                   Product__c = dc.Product__c 
                ));                 
            }            
            insert dcToInsert;             
            isSaved = true;
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            Database.rollback(sp);
        }
        return isSaved;
    }
    
    public PageReference Save() {       
        pageReference pr;
        if (SaveNow()) {
            pr = new pageReference('/' + cloneContract.Id);
        }
        return pr;
    }
    
    public PageReference SaveAndNew() {
        pageReference pr;
        if (SaveNow()) {
            pr = new pageReference('/a00/e?retURL=%2Fa00%2Fo');
        }
        return pr;
    }
    
    public PageReference NewDC() {
        return new PageReference('/apex/DiscountCommisionEdit?CF00N90000004nJO8=' + dcContract.Name + '&CF00N90000004nJO8_lkid='+ dcContract.Id +'&scontrolCaching=1&retURL=%2F'+ dcContract.Id +'&sfdc.override=1');        
    }
    
    public void EditSelectedDC() {    
        SelectedDCs = new List<Discount_Commission__c>();
        for (Discount_Commission__c dc : DCs) {
            if (dc.selected__c) {
                dc.selected__c = false;
                SelectedDCs.add(dc);
            }    
        }   
        isDCEdit = true; 
    }        
    
    public void UpdateDCs() {
        try {
            update SelectedDCs;
            CancelEdit();    
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));         
        }
    }
    
    public void CancelEdit() {
        isDCEdit = false;
    }
    
    public void DeleteAllDC() {
        try {            
            List<Discount_Commission__c> dcList = [Select Id From Discount_Commission__c Where Discount_Commission_Contract__c = :dcContract.Id]; 
            if (!dcList.isEmpty()) {
                Delete dcList;    
            }
            GetDCRecords();
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));         
        }
    }
    
    public void DeleteDC() {
        try {
            String dcid = Apexpages.currentPage().getParameters().get('dcid');
            Delete [Select Id From Discount_Commission__c Where Id = :dcid]; 
            GetDCRecords();
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));         
        }
    }
    
    public List<SelectOption> getPricebooks() {
        List<SelectOption> option = new List<SelectOption>();
        for (Pricebook2 p : [Select Id, Name From Pricebook2 Where IsStandard = false and IsActive = true]) {
            option.add(new SelectOption(p.Id, p.Name));
        }
        return option;
    } 
    
    private String getDCQuery(Boolean isCount) {
        String query = '';
        if (isCount) {
            query = 'Select count() From Discount_Commission__c Where Discount_Commission_Contract__c = \'' + dcContract.Id + '\'';
        } else {
            query = 'Select selected__c, Id, Name, Product__c, Product__r.Name, Price_Book__c, Price_Book_Name__c, Customer_Discount__c, Affiliate_Commision__c From Discount_Commission__c Where Discount_Commission_Contract__c = \'' + dcContract.Id + '\'';
        }            
        
        if (searchText != '') {
            query += ' and (Name like \'%' + searchText + '%\' or Product__r.Name like \'%' + searchText + '%\' or Price_Book_Name__c like \'%' + searchText + '%\')';
        }
        return query;
    }
    public void GetDCRecords() {
        noOfRecords = Database.countQuery(getDCQuery(true));
        pageNumber = 1;                     
        totalPages = Integer.valueOf(math.ceil((double)noOfRecords/(double)pagesize ));        
        
        // Process Invoice
        ProcessDCRecords();
    }
    
    public void ProcessDCRecords() {
        DCs = new List<Discount_Commission__c>();
                
        String sortVal = sortExpression + ' ' + sortDirection;
        String queryString = getDCQuery(false) + ' Order By ' + sortVal;
        Integer offset = (pageNumber-1) * pageSize;                                     
        if (offset < 2001) {
            queryString += ' LIMIT ' +  pageSize + ' OFFSET '+ offset;
            DCs = Database.query(queryString);
        } else {
            Integer max = pageSize + offset;    
            queryString += ' LIMIT ' +  max;
            if (max > noOfRecords) max = noOfRecords; 
            List<Discount_Commission__c> temp = Database.query(queryString);
            for (Integer i=offset; i<max; i++) {
                DCs.add(temp[i]);
            }
        }
    }
    
    public String CurrentPageNumber {
        get { return String.valueOf(pageNumber); }
        set { pageNumber = Integer.valueOf(value); }
    }
    
    public List<SelectOption> getPages() {
        List<SelectOption> options = new List<SelectOption>();                     
        for (Integer i = 1; i<= totalPages; i++){
            options.add(new SelectOption(String.valueof(i),String.valueof(i)));
        }
                
        return options;
    }
    
    public Boolean hasNext {
        get { 
            return pageNumber < totalPages; 
        }
        set;
    }
    
    public Boolean hasPrevious {
        get { 
            return pageNumber > 1; 
        }
        set;
    }      
    
    public void first() {
        pageNumber = 1;                            
        ProcessDCRecords();    
    }
    
    public void last() {
        pageNumber = totalPages;                
        ProcessDCRecords();
    }
    
    public void previous() {
        if (hasPrevious) {
            pageNumber--;                       
            ProcessDCRecords();
        }
    }
    
    public void next() {
        if (hasNext) {
            pageNumber++;                       
            ProcessDCRecords();    
        }
    }
    
    public String getSortExpression(){
       return sortExpression;
    }
    
    public void setSortExpression(String value){
        if (value == sortExpression){
            sortDirection = (sortDirection=='ASC') ? 'DESC' : 'ASC';
        }else{
            sortDirection = 'ASC';
        }
               
        sortExpression = value;
    }
    
    public String getSortDirection(){
        if (sortExpression== null || sortExpression == ''){
            return 'ASC';
        }else{
            return sortDirection;
        }
    }
    
    public void setSortDirection(String value){
        sortDirection = value;
    }
    
    public static testMethod void testDCContractExtension() {
    	Account act = new Account(Name='test');
    	insert act;
    	    	
    	Discount_Commission_Contract__c dcc = new Discount_Commission_Contract__c(Name='test contract', Affiliate_Partner__c=act.Id, Start_Date__c=date.today(), End_Date__c=date.today(), Type__c='Campaign');
    	insert dcc;	
    	
    	Product2 p = new Product2(Name='prod', ProductCode='prod', Family='Add-ons', isActive=true);
    	insert p;
    	
    	//Pricebook2 pbook = new Pricebook2(Name='Test', IsActive=true);
		//insert pbook;
		Pricebook2 pbook = [select id from Pricebook2 where IsStandard = true limit 1];
		
		Discount_Commission__c dc = new Discount_Commission__c(Product__c=p.Id, PriceBookId__c=pbook.Id, Customer_Discount__c=1, Affiliate_Commision__c=1, Discount_Commission_Contract__c=dcc.Id);
    	insert dc;
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(dcc);
    	DCContractExtension con = new DCContractExtension(sc);
    	
    	con.Save();
    	con.SaveAndNew();
    }
}