public class DiscountCommisionExtension {
    public Boolean isEdit {get; set;}    
    private Discount_Commission__c discom {get; set;}
    private ApexPages.StandardController con;
    public DiscountCommisionExtension(ApexPages.StandardController controller) {
        this.discom = (Discount_Commission__c)controller.getRecord();
        con = controller;    
        if (this.discom.Id == null) {
            this.discom = new Discount_Commission__c();    
            isEdit = false;
        } else {
            isEdit = true;           
        }
    }
    
    public List<SelectOption> getPricebooks() {
        List<SelectOption> option = new List<SelectOption>();
        for (Pricebook2 p : [Select Id, Name From Pricebook2 Where IsStandard = false and IsActive = true]) {
            option.add(new SelectOption(p.Id, p.Name));
        }
        return option;
    }      
    
    public PageReference SaveAndNew() {
        PageReference pr;
        try {
            con.save();
            Discount_Commission__c dc = (Discount_Commission__c)con.getRecord();            
            pr = new PageReference('/apex/DiscountCommisionEdit?CF00N90000004nJO8=' + dc.Discount_Commission_Contract__c + '&CF00N90000004nJO8_lkid='+ dc.Discount_Commission_Contract__c +'&scontrolCaching=1&retURL=%2F'+ dc.Discount_Commission_Contract__c +'&sfdc.override=1');            
            pr.setRedirect(true);            
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        return pr;
    }             
}