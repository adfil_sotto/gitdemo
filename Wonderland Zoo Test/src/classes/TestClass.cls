@isTest(SeeAllData=true)
public class TestClass {
    
    static testmethod void testDiscountCommisionExtension() {
        Account act = new Account(Name='test');
        insert act;
        
        Account act2 = new Account(Name='test2');
        insert act2;
                
        Discount_Commission_Contract__c dcc = new Discount_Commission_Contract__c(Name='test contract', Affiliate_Partner__c=act.Id, Start_Date__c=date.today(), End_Date__c=date.today(), Type__c='Campaign');
        insert dcc;
        
        Discount_Commission_Contract__c dcc2 = new Discount_Commission_Contract__c(Name='test contract', Affiliate_Partner__c=act.Id, Start_Date__c=date.today().addDays(1), End_Date__c=date.today().addDays(3), Type__c='Campaign');
        insert dcc2;
        
        dcc2.Start_Date__c=date.today().addDays(2);
        update dcc2;
        
        Product2 p = new Product2(Name='prod', ProductCode='prod', Family='Add-ons', isActive=true);
        insert p;
        
        Product2 p2 = new Product2(Name='prod2', ProductCode='prod2', Family='Add-ons', isActive=true);
        insert p2;
        
        Product2 p3 = new Product2(Name='prod2', ProductCode='prod2', Family='Add-ons', isActive=true);
        insert p3;
                
        Pricebook2 pbook = new Pricebook2(Name='Test', IsActive=true);
        insert pbook;    
        
        Pricebook2 pbooks = [select id from Pricebook2 where IsStandard = true limit 1];                              
        
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=pbooks.Id, Product2Id=p.Id, UnitPrice=1, UseStandardPrice=false, IsActive=true);
        insert pbe;
        
        PricebookEntry pbe2 = new PricebookEntry(Pricebook2Id=pbooks.Id, Product2Id=p2.Id, UnitPrice=1, UseStandardPrice=false, IsActive=true);
        insert pbe2;
        
        PricebookEntry pbe3 = new PricebookEntry(Pricebook2Id=pbooks.Id, Product2Id=p3.Id, UnitPrice=1, UseStandardPrice=false, IsActive=true);
        insert pbe3;
        
        Discount_Commission__c dc = new Discount_Commission__c(Product__c=p.Id, PriceBookId__c=pbooks.Id, Customer_Discount__c=1, Affiliate_Commision__c=1, Discount_Commission_Contract__c=dcc.Id);
        insert dc;
        
        Discount_Commission__c dc2 = new Discount_Commission__c(Product__c=p2.Id, PriceBookId__c=pbook.Id, Customer_Discount__c=1, Affiliate_Commision__c=1, Discount_Commission_Contract__c=dcc.Id);
        insert dc2;
        
        Discount_Commission__c dc3 = new Discount_Commission__c(Product__c=p3.Id, PriceBookId__c=pbook.Id, Customer_Discount__c=1, Affiliate_Commision__c=1, Discount_Commission_Contract__c=dcc.Id);
        insert dc3;

        Opportunity op = new Opportunity(Name='Test', AccountId=act.Id, StageName='Pending Payment', CloseDate=date.today(), Discount_Commission_Contract__c=dcc.Id);
        insert op;
        
        OpportunityLineItem opItem = new OpportunityLineItem(OpportunityId=op.Id, PriceBookEntryId=pbe.Id, Discount_Commission__c=dc.Id, Quantity=1, TotalPrice=1);
        insert opItem;
        
        List<Opportunity> opps = new List<Opportunity>(); 
        Opportunity op1 = new Opportunity(Name='Test1', AccountId=act.Id, StageName='Pending Payment', CloseDate=date.today().addDays(1), Discount_Commission_Contract__c=dcc.Id);
        opps.add(op1);
                
        Opportunity op2 = new Opportunity(Name='Test2', AccountId=act.Id, StageName='Pending Payment', CloseDate=date.today().addDays(2), Discount_Commission_Contract__c=dcc.Id);
        opps.add(op2);
        
        insert opps;
        
        List<OpportunityLineItem> oppItems = new List<OpportunityLineItem>(); 
        OpportunityLineItem opItem2 = new OpportunityLineItem(OpportunityId=op.Id, PriceBookEntryId=pbe2.Id, Discount_Commission__c=dc2.Id, Quantity=1, TotalPrice=1);
        oppItems.add(opItem2);
        
        OpportunityLineItem opItem3 = new OpportunityLineItem(OpportunityId=op.Id, PriceBookEntryId=pbe3.Id, Discount_Commission__c=dc3.Id, Quantity=1, TotalPrice=1);
        oppItems.add(opItem3);               
        
        insert oppItems;
        
        opps[0].Discount_Commission_Contract__c = null;                
        opps[1].CloseDate = date.today();
        update opps;
                        
        dc.Product__c = p2.Id;
        update dc;
        
        dc2.Customer_Discount__c = 2;
        update dc2;
         
        ApexPages.StandardController sc = new ApexPages.StandardController(dc);
        DiscountCommisionExtension con = new DiscountCommisionExtension(sc);
        con.getPricebooks();
        con.SaveAndNew();
        
        Discount_Commission__c dcnew = new Discount_Commission__c();
        ApexPages.StandardController sc2 = new ApexPages.StandardController(dcnew);
        DiscountCommisionExtension con2 = new DiscountCommisionExtension(sc2);
    }
    
    static testMethod void testDCContractExtension() {
        Account act = new Account(Name='test');
        insert act;
                
        Discount_Commission_Contract__c dcc = new Discount_Commission_Contract__c(Name='test contract', Affiliate_Partner__c=act.Id, Start_Date__c=date.today(), End_Date__c=date.today(), Type__c='Campaign');
        insert dcc; 
        
        Product2 p = new Product2(Name='prod', ProductCode='prod', Family='Add-ons', isActive=true);
        insert p;
        
        Product2 p2 = new Product2(Name='prod2', ProductCode='prod2', Family='Add-ons', isActive=true);
        insert p2;
        
        Pricebook2 pbook2 = new Pricebook2(Name='Test', IsActive=true);
        insert pbook2;
        
        Pricebook2 pbook = [select id from Pricebook2 where IsStandard = true limit 1];
        
        Discount_Commission__c dc = new Discount_Commission__c(selected__c=true, Product__c=p.Id, PriceBookId__c=pbook.Id, Customer_Discount__c=1, Affiliate_Commision__c=1, Discount_Commission_Contract__c=dcc.Id);
        insert dc;
        
        Discount_Commission__c dc2 = new Discount_Commission__c(selected__c=true,Product__c=p2.Id, PriceBookId__c=pbook.Id, Customer_Discount__c=1, Affiliate_Commision__c=1, Discount_Commission_Contract__c=dcc.Id);
        insert dc2;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(dcc);
        DCContractExtension con = new DCContractExtension(sc);
        
        con.GetDCRecords();
        con.CurrentPageNumber = '1';
        String cp = con.CurrentPageNumber;
        Boolean next = con.hasNext;
        Boolean prev = con.hasPrevious;
        con.getPages();
        con.first();
        con.last();
        con.previous();
        con.next();
        con.getSortExpression();
        con.setSortExpression('Name');
        con.getSortDirection();
        con.setSortDirection('ASC');
        con.getPricebooks();
        con.EditSelectedDC();
        con.CancelEdit();
        con.UpdateDCs();
        con.DeleteDC();
        con.DeleteAllDC();
        con.Save();
        con.SaveAndNew();
        con.NewDC();
    }
}