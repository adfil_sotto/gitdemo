trigger UpdateRelatedOppItems on Discount_Commission__c (before delete, after insert, after update) {
	// insert
	if (trigger.isInsert) {
		// apply to OpportunityLineItems
		ApplyDCContractClass.ApplyDCToOppItems(trigger.new);		
	} 
	
	// update
	if (trigger.isUpdate) {
		Integer index = 0;
		Map<Id, Discount_Commission__c> MapDCPPChange = new Map<Id, Discount_Commission__c>();
		Map<Id, Discount_Commission__c> mapDCDisChange = new Map<Id, Discount_Commission__c>(); 
		for (Discount_Commission__c dc : trigger.new) {
			// product or pricebook has changed
			if (dc.Product__c != trigger.old[index].Product__c || dc.PriceBookId__c != trigger.old[index].PriceBookId__c) {
				MapDCPPChange.put(dc.Id, dc);
			} else {
				// Customer discount or Affiliate commision has changed
				if (dc.Customer_Discount__c != trigger.old[index].Customer_Discount__c || dc.Affiliate_Commision__c != trigger.old[index].Affiliate_Commision__c) {
					mapDCDisChange.put(dc.Id, dc);		
				}
			}
			index++;
		}
				
		// discounts changed
		if (!mapDCDisChange.isEmpty()) {
			List<OpportunityLineItem> oppItemsToUpdate = new List<OpportunityLineItem>();				 
			// get related opplineitems
			for (OpportunityLineItem opp : [Select Id, Discount, Discount_Commission__c, Description, Affiliate_Commision__c, Customer_Discount__c From OpportunityLineItem Where Discount_Commission__c in :mapDCDisChange.keySet()]) {
				Discount_Commission__c dc = mapDCDisChange.get(opp.Discount_Commission__c);
				// set discount to default value
				if (opp.Discount != null) {
					if (opp.Affiliate_Commision__c != null) {
						opp.Discount -= opp.Affiliate_Commision__c;
					}
					if (opp.Customer_Discount__c != null) {
						opp.Discount -= opp.Customer_Discount__c;
					}
				}
				
				if (opp.Discount < 0) {
					opp.Discount = 0;
				}
				String descr = '';
				// affiliate commision
                if (dc.Affiliate_Commision__c != null && dc.Affiliate_Commision__c != 0) {
                	opp.Discount += dc.Affiliate_Commision__c;                        
                    descr = 'Affiliate Commision = ' + String.valueOf(dc.Affiliate_Commision__c); 
                }	
                
                // customer discount
                if (dc.Customer_Discount__c != null && dc.Customer_Discount__c != 0) {
                    opp.Discount += dc.Customer_Discount__c;                        
                    if (descr != '') descr+= '; '; 
                    descr += 'Customer Discount = ' + String.valueOf(dc.Customer_Discount__c);
                }                            
            	opp.Description = descr;                	
            	opp.Affiliate_Commision__c = dc.Affiliate_Commision__c;
            	opp.Customer_Discount__c = dc.Customer_Discount__c;
            	
            	oppItemsToUpdate.add(opp);				
			}
			// update here
			if (!oppItemsToUpdate.isEmpty()) {
				update oppItemsToUpdate;
			}
		}
		
		// product or pricebook has changed
		if (!MapDCPPChange.isEmpty()) {
			// remove opportunitylineitems dc lookup
			ApplyDCContractClass.RemoveOppItemsDCByDC(MapDCPPChange.keySet());
						
			// apply to OpportunityLineItems
			ApplyDCContractClass.ApplyDCToOppItems(MapDCPPChange.values());
		}
	} 
	
	// delete
	if (trigger.isDelete) {
		System.debug('trigger.oldMap.keySet() : ' + trigger.oldMap);
		// remove opportunitylineitems dc lookup
		ApplyDCContractClass.RemoveOppItemsDCByDC(trigger.oldMap.keySet());		
	}
}