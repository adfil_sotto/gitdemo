trigger preventDuplicatePriceBookProduct on Discount_Commission__c (before insert, before update) {
    List<Discount_Commission__c> dcToProcess = new List<Discount_Commission__c>();
    Map<Id, Set<String>> mapEPbProd = new Map<Id, Set<String>>();
    Set<Id> setContractIds = new Set<Id>();
    Boolean hasSomethingTovalidate = false;
    if (trigger.isInsert) {
        Map<Id, Set<String>> mapPbProd = new Map<Id, Set<String>>();        
        for (Discount_Commission__c dc : trigger.new) {
            if (dc.PriceBookId__c != null && dc.PriceBookId__c != '' && dc.Product__c != null) {            
                setContractIds.add(dc.Discount_Commission_Contract__c);    
                dcToProcess.add(dc);
            }
        }
        
        hasSomethingTovalidate = true;
                
    } else { // isUpdate
        Integer index = 0;        
        for (Discount_Commission__c dc : trigger.new) {
            if (dc.Product__c != trigger.old[index].Product__c || dc.PriceBookId__c != trigger.old[index].PriceBookId__c) {
                setContractIds.add(dc.Discount_Commission_Contract__c);
                dcToProcess.add(dc);
            }
            index++;
        }
        
        hasSomethingTovalidate = !setContractIds.isEmpty();                                            
    }
    
    if (hasSomethingTovalidate) {    
        for (Discount_Commission__c dc : [Select Discount_Commission_Contract__c, PriceBookId__c, Product__c From Discount_Commission__c Where Discount_Commission_Contract__c in :setContractIds]) {
            if (mapEPbProd.get(dc.Discount_Commission_Contract__c) == null) {
                mapEPbProd.put(dc.Discount_Commission_Contract__c, new Set<String>());
            }
            mapEPbProd.get(dc.Discount_Commission_Contract__c).add(dc.PriceBookId__c+dc.Product__c);
        }
        
        for (Discount_Commission__c dc : dcToProcess) {            
            if (mapEPbProd.containsKey(dc.Discount_Commission_Contract__c)) {
                if (mapEPbProd.get(dc.Discount_Commission_Contract__c).contains(dc.PriceBookId__c+dc.Product__c)) {
                    dc.addError('Product & Pricebook already exist on the same contract!');    
                }
            }
        }
    }
}