trigger preventOverlappingDate on Discount_Commission_Contract__c (before insert, before update) {
    // this only works on per record basis
    if (trigger.new.size() == 1) {
        List<Discount_Commission_Contract__c> dcContracts;
        if (trigger.isInsert) {
            dcContracts = [Select Id From Discount_Commission_Contract__c 
                               Where Affiliate_Partner__c = :trigger.new[0].Affiliate_Partner__c and 
                                     Type__c = :trigger.new[0].Type__c and 
                                     ((Start_Date__c <= :trigger.new[0].Start_Date__c and End_Date__c >= :trigger.new[0].Start_Date__c) or
                                      (Start_Date__c <= :trigger.new[0].End_Date__c and End_Date__c >= :trigger.new[0].End_Date__c))];                                                                                     
        } else { // update
           dcContracts = [Select Id From Discount_Commission_Contract__c 
                               Where Id != :trigger.new[0].Id and 
                                     Affiliate_Partner__c = :trigger.new[0].Affiliate_Partner__c and 
                                     Type__c = :trigger.new[0].Type__c and 
                                     ((Start_Date__c <= :trigger.new[0].Start_Date__c and End_Date__c >= :trigger.new[0].Start_Date__c) or
                                      (Start_Date__c <= :trigger.new[0].End_Date__c and End_Date__c >= :trigger.new[0].End_Date__c))];     
        }   
        
        if (!dcContracts.isEmpty()) {
            trigger.new[0].addError('Overlapping dates found on same Type and Affiliate Partner.');        
        } 
    }        
}